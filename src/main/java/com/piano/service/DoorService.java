package com.piano.service;

import com.piano.persistence.entity.Room;
import com.piano.persistence.entity.User;
import com.piano.persistence.repository.UserRepository;
import com.piano.service.exception.DoorServiceException;
import com.piano.service.exception.UserAccessDeniedException;
import com.piano.service.exception.UserNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@RequiredArgsConstructor
public class DoorService {

    private final UserRepository userRepository;

    @Transactional
    public void enter(int roomId, int userId, boolean entrance) {
        log.debug("Try to enter room. User={}, room={}, entrance={}", userId, roomId, entrance);

        User user = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException(String.format("User doesn't exists [%s]", userId)));

        if (isUserNotAllowEnter(user, roomId)) {
            throw new UserAccessDeniedException(
                    String.format("The user [%s] cannot enter to the door [%s]", userId, roomId));
        }

        if (entrance) {
            if (user.getRoom() != null) {
                throw new DoorServiceException(
                        String.format("User [%s] already into room [%s]", userId, user.getRoom().getId()));
            } else {
                Room room = new Room(roomId);
                user.setRoom(room);
            }
            log.debug("The user {} entered to room {}.", userId, roomId);
        } else {
            user.setRoom(null);
            log.debug("The user {} went out from room {}.", userId, roomId);
        }
    }

    private boolean isUserNotAllowEnter(User user, int roomId) {
        return user.getId() % roomId != 0;
    }

}
