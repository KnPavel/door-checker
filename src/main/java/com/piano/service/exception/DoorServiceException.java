package com.piano.service.exception;

/**
 * @author <a href="mailto:pknyazev@wiley.com">Pavel Knyazev</a>
 */
public class DoorServiceException extends RuntimeException {

    public DoorServiceException(String message) {
        super(message);
    }
}
