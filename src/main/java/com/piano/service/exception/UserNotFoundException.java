package com.piano.service.exception;

/**
 * @author <a href="mailto:pknyazev@wiley.com">Pavel Knyazev</a>
 */
public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException(String message) {
        super(message);
    }
}
