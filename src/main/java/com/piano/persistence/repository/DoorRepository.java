package com.piano.persistence.repository;

import com.piano.persistence.entity.Room;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DoorRepository extends CrudRepository<Room, Integer> {
}
