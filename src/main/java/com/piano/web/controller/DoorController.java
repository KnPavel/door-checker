package com.piano.web.controller;

import com.piano.service.DoorService;
import com.piano.service.exception.DoorServiceException;
import com.piano.service.exception.UserAccessDeniedException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/check")
public class DoorController {

    private final DoorService doorService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public void enter(@RequestParam("roomId") int roomId,
                      @RequestParam("entrance") boolean entrance,
                      @RequestParam("keyId") int keyId) {
        doorService.enter(roomId, keyId, entrance);
    }

    @ResponseBody
    @ExceptionHandler({UserAccessDeniedException.class, DoorServiceException.class})
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public String accessDenied(RuntimeException exception) {
        String msg = exception.getMessage();
        log.debug(msg);
        return msg;
    }

}
