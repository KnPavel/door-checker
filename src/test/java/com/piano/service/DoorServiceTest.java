package com.piano.service;

import com.piano.persistence.entity.Room;
import com.piano.persistence.entity.User;
import com.piano.persistence.repository.UserRepository;
import com.piano.service.exception.DoorServiceException;
import com.piano.service.exception.UserAccessDeniedException;
import com.piano.service.exception.UserNotFoundException;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;

@ExtendWith(MockitoExtension.class)
class DoorServiceTest {

    private static final int ROOM_ID_1 = 1;
    private static final int ROOM_ID_2 = 2;
    private static final int USER_1 = 1;
    private static final boolean ENTRANCE_IN = true;
    private static final boolean ENTRANCE_OUT = false;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private DoorService doorService;


    @Test
    public void testUserNotFound() {
        doReturn(Optional.empty()).when(userRepository).findById(USER_1);

        assertThrows(UserNotFoundException.class, () -> doorService.enter(ROOM_ID_1, USER_1, ENTRANCE_IN));
    }

    @Test
    public void testUserAccessDenied() {
        User user = new User(USER_1, null);
        doReturn(Optional.of(user)).when(userRepository).findById(USER_1);

        assertThrows(UserAccessDeniedException.class, () -> doorService.enter(ROOM_ID_2, USER_1, ENTRANCE_IN));
    }

    @Test
    public void testUserAlreadyIntoRoom() {
        Room room = new Room(ROOM_ID_1);
        User user = new User(USER_1, room);

        doReturn(Optional.of(user)).when(userRepository).findById(USER_1);

        assertThrows(DoorServiceException.class, () -> doorService.enter(ROOM_ID_1, USER_1, ENTRANCE_IN));
    }

    @Test
    public void testUserWentToRoomSuccess() {
        User user = new User(USER_1, null);

        doReturn(Optional.of(user)).when(userRepository).findById(USER_1);

        doorService.enter(ROOM_ID_1, USER_1, ENTRANCE_IN);

        Room expectedRoom = new Room(ROOM_ID_1);
        assertEquals(expectedRoom, user.getRoom());
    }

    @Test
    public void testUserWentOutFromRoomSuccess() {
        Room room = new Room(ROOM_ID_1);
        User user = new User(USER_1, room);

        doReturn(Optional.of(user)).when(userRepository).findById(USER_1);

        doorService.enter(ROOM_ID_1, USER_1, ENTRANCE_OUT);

        assertNull(user.getRoom());
    }
}























